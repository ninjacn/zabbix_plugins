#!/usr/bin/python
#check redis
#yaopengming@xiaomi.com 01/25/2014

import redis
import sys

def usages():
    print("Usages: ./redis.py host port read|write|info args")

def main():
#    try:
        if len(sys.argv) < 4:
            usages()
            sys.exit()
    
        if sys.argv[3]=="read":
            zabbix_key="zabbix_read"
            pool = redis.ConnectionPool(host=sys.argv[1], port=sys.argv[2], db=0)
            r = redis.Redis(connection_pool=pool)
            if r.get(zabbix_key):
                print(1)
            else:
                print(0)

        elif sys.argv[3]=="write":
            zabbix_key="zabbix_write"
            pool = redis.ConnectionPool(host=sys.argv[1], port=sys.argv[2], db=0)
            r = redis.Redis(connection_pool=pool)
            r.delete(zabbix_key)
            if r.set(zabbix_key,1):
                print(1)
            else:
                print(0)
        elif sys.argv[3]=="info":
            pool = redis.connection.ConnectionPool(host=sys.argv[1], port=sys.argv[2], db=0)
            r = redis.Redis(connection_pool=pool)
            try:
                if sys.argv[4]=="used_memory_rss":
                    print "%.2f" % (float(r.info()["used_memory_rss"])/1024/1024/1024)
                else:
                    print r.info()[sys.argv[4]]
            except:
                print r.info()
#    except:
#        print 0

if __name__ == '__main__':
    main()

