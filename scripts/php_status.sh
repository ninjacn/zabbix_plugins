#!/bin/bash

# ninjacn
# 14/08/15

HOST="127.0.0.1"
PORT="80"

function idle_processes {
    /usr/bin/curl -s "http://$HOST:$PORT/php-fpm-status" 2>/dev/null| grep 'idle processes' |awk '{print $NF}'
    }

function active_processes {
    /usr/bin/curl -s "http://$HOST:$PORT/php-fpm-status" 2>/dev/null| grep 'active processes' |awk 'NR==1{print $NF}'
    }

function max_active_processes {
    /usr/bin/curl -s "http://$HOST:$PORT/php-fpm-status" 2>/dev/null| grep 'max active processes' |awk '{print $NF}'
    }

function total_processes {
    /usr/bin/curl -s "http://$HOST:$PORT/php-fpm-status" 2>/dev/null|  grep 'total processes' |awk '{print $NF}'
    }

function listen_queue {
    /usr/bin/curl -s "http://$HOST:$PORT/php-fpm-status" 2>/dev/null| grep 'listen queue' |awk 'NR==1{print $NF}'
    }

function max_listen_queue {
    /usr/bin/curl -s "http://$HOST:$PORT/php-fpm-status" 2>/dev/null| grep 'max listen queue' |awk '{print $NF}'
    }

function php-fpm_ping {
    result="$(/usr/bin/curl -s http://${HOST}:${PORT}/php-fpm-ping 2>/dev/null)"
    if [ "$result" == "pong" ]
    then
        echo 1
    else
        echo 0
    fi
    }

function process_number {
    ps -ef|grep php-fpm|grep -v grep|wc -l
    }

$1

