#!/bin/env python
# -*- coding: utf-8 -*-
# 获取nutcracker状态
# yaopm@1mxian.com 08/12/2015

import sys
import telnetlib
import json

def main():
    try:
        host = sys.argv[1]
        port = sys.argv[2]
        key = sys.argv[3]
        t = telnetlib.Telnet(host, port)
        stat = t.read_all()
        t.close()
        print json.loads(stat)[key]
        # print json.loads(stat)["curr_connections"]
    except:
        print None


if __name__ == '__main__':
    main()
