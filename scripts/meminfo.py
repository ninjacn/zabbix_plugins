#!/usr/bin/env python

import commands
import sys

def meminfo():
    result = dict()
    fd_r = open('/proc/meminfo','r')
    for line in fd_r.read().split('\n'):
        if line:
            key,value = line.split(':')
            data = value.split()[0]
        result[key] = int(data)

    mfree = result['MemFree']+result['Buffers']+result['Cached']
    pfree = "%.4f" %(float(mfree)/result['MemTotal'])
    result['pfree'] = pfree
    fd_r.close
    return result


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print 0
		sys.exit(1)

	mem = meminfo()

	try:
		print mem[sys.argv[1]]
	except:
		print 0
