#!/bin/bash

#队列监控
#ninjacn 06/08/15

export PATH=$PATH:/home/work/app/redis/bin
REDIS_CLI="redis-cli"
queue_name=$1
echo `"${REDIS_CLI}" LLEN queues:"${queue_name}"`
